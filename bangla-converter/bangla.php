
<?php 

include("bangconvert.php"); 

$iscii=$_POST["iscii"];
/* $filepath=$_FILES['tmp_name'];
$filename=$_POST['filename'];  */
$n=strlen($iscii); 
$x=""; 

// We need to look ahead two, as preceding vowels in Sutonny may 
// need to shift two positions if the following character is just 
// a half character 

$i=0; 
while ($i<$n)
{ 
	$j=ord(substr($iscii,$i,1)); 
	if ($j==119 or ($j>133 and $j<138))
	{ 
		$i++; 
		$c2=substr($iscii,$i,1); 
		$c3=substr($iscii,$i+1,1); 
		$j2=ord($c2); 
		$j3=ord($c3); 
		$x=$x.$c2; 
		if (($j2>147 and $j2<150) or ($j2>151 and $j2<156) or $j2==164 or $j2==159 or $j2==174 or $j2==175 or ($j3>164 and $j3<173))
		{ 
			$i++; 
			$x=$x.$c3; 
		} // complete the case of passing two characters 
	} // complete the case of passing one or two characters 
	$x=$x.chr($j); 
	$i++;
} 

/* Next, run through and move 169 back one character */ 

$xx=$x; 
$x=""; 
$i=0; 
$cr=chr(169); 
while ($i<$n)
{ 
	$c1=substr($xx,$i,1); 
	$c2=substr($xx,$i+1,1); 
	if ($c2==$cr)
	{
		$x=$x.$c2.$c1;
		$i++;
		$i++;
	}
	else
	{
		$x=$x.$c1;
		$i++;
	} 
}

/* Next, do the actual conversion from iscii to unicode */ 

for ($i=0;$i<$n;$i++)
{ 
	$j=ord(substr($x,$i,1)); 
	$u=$u.bangconvert($j); 
} 
$u=str_replace("\n","<br>",$u); 
$u=str_replace("&#2437;&#2494;","&#2438;",$u); // change a+line to aa 
$u=str_replace("&#2433;&#2494;","&#2494;&#2433;",$u); //swap bindi and line 
$u=str_replace("&#2433;&#2497;","&#2497;&#2433;",$u); //swap bindi and loop 
$u=str_replace("&#2503;&#2494;","&#2507;",$u); //combine curve and line 
$u=str_replace("&#2504;&#2494;","&#2508;",$u); //combine flag curve and line 
$u=str_replace("&#250;&#2494;","&#2508;",$u); //combine flag curve and line 
// $u=str_replace("&#2472;-","&#2472;&#2509;&#2468;",$u); 
$u=str_replace("&#2472;&#2509;&#2478;&#2497;&#2497;","&#2472;&#2509;&#2478;&#2497;",$u); 
$u=str_replace("&#2495;&#2509;&#2480;","&#2509;&#2480;&#2495;",$u); //move r inside the big loop 
$u=str_replace("&#2486;&#2480;&#2509;&#2496;&#2487;","&#2486;&#2496;&#2480;&#2509;&#2487;",$u); //triple whammy 
$u=str_replace("&#2476;&#2480;&#2509;&#2494;","&#2480;&#2509;&#2476;&#2494;",$u); //triple whammy 
$u=str_replace("&#2474;&#2495;&#2509;&#2482;","&#2474;&#2509;&#2482;&#2495;",$u); // 5-05 roobon error fix 
$u=str_replace("&#2486;&#2495;&#2509;&#2482;","&#2486;&#2509;&#2482;&#2495;",$u); // ditto 

// fixup - make sure curve is after any other combinant 
$ic=strpos($u,"&#2503;&#2509;"); 
while ($ic>0)
{ 
	
	$u=substr($u,0,$ic).substr($u,$ic+7,14)."&#2503;".substr($u,$ic+21); 
	$ic=strpos($u,"&#2503;&#2509;"); 
}
//header( 'Content-type: text/html; charset=utf-8' ); 
?> 
<html> 
<head><title>Bangla Unicode Output</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
</head> 
<body> 
<?=$u?>
<hr><p> Original Text: <span lang=BN style='font-size:18.0pt;font-family: SutonnyMJ;'><?=$iscii?></span></p>
<hr><p> Converted Text: <span lang=BN style='font-size:18.0pt;font-family: TonnyBanglaMJ;'><?=$u?></span></p>
<hr><p> Converted Text: <span style='font-size:18.0pt;'><?=$u?></span></p>

<? 
	$str="";
	for ($i=0;$i<9;$i++)
	{
		$str.=ord(substr($iscii,$i,1))." ";
	} 
?>   
<hr>
<p>  <div align="center"><br>
   Converted Unicode Text:<br><? //=$str?>
   <textarea name="output" cols="75" rows="10"><? echo $u?></textarea></p>
   <? 


 ?> 
  
 <br>
   </p>
 </div>
</body> 
</html> 
