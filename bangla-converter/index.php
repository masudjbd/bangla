<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0034)http://local-democracy.org/bangla/ -->
<HTML><HEAD><TITLE>Bangla Unicode Converter Part 1</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<META content="MSHTML 6.00.2900.2912" name=GENERATOR></HEAD>
<BODY>
<H2 align="center"><I><FONT face=Arial color=#008000>Cut and paste some SutonnyMJ Bangla into 
this box</FONT></I></H2>
<FORM action=bangla.php method=post>
  <div align="center">
    <TEXTAREA style="FONT-SIZE: 20pt; FONT-FAMILY: SutonnyMJ" name=iscii rows=8 cols=60></TEXTAREA> 
  
</div>
  <P align="center"><INPUT type=submit value="Convert and display in Unicode"> </P>
</FORM>
<P class=MsoPlainText><B><I><FONT face=Arial size=2>I<SPAN 
style="FONT-SIZE: 10pt">nstructions:</SPAN></FONT></I></B></P>
<OL>
  <LI>
  <P class=MsoPlainText 
  style="MARGIN-LEFT: 0.5in; TEXT-INDENT: -0.25in; LINE-HEIGHT: 200%"><FONT 
  face=Arial>Open a Sutonny Bangla Word document in WordPad (not Word! Word will 
  damage the Bangla when you copy it.). To do that right click the document, and 
  select "Open with.." and "WordPad" from the menu. </FONT></P>
  <LI>
  <P class=MsoPlainText 
  style="MARGIN-LEFT: 0.5in; TEXT-INDENT: -0.25in; LINE-HEIGHT: 200%"><FONT 
  face=Arial>Select Bangla text to be converted from WordPad document and paste 
  it into the box above. </FONT></P>
  <LI>
  <P class=MsoPlainText 
  style="MARGIN-LEFT: 0.5in; TEXT-INDENT: -0.25in; LINE-HEIGHT: 200%"><FONT 
  face=Arial>Click the button "Convert and display in Unicode". In a few 
  moments, you will see the converted text.</FONT> </P>
  <LI>
  <P class=MsoPlainText 
  style="MARGIN-LEFT: 0.5in; TEXT-INDENT: -0.25in; LINE-HEIGHT: 200%"><FONT 
  face=Arial>Save the resulting webpage for further use. You can just copy and 
  paste the text from here in to your developing web page.</FONT> </P></LI></OL>
<P class=MsoPlainText style="MARGIN-LEFT: 0.5in; TEXT-INDENT: -0.25in"><FONT 
face=Arial>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
&nbsp;"If you are using Office 2003, then just copy the text directly from 
MSWord and paste it into the box above. No need to open with 
WordPad."</FONT></P>
<HR>

<P><I>(c) 2004-2005, John Coonrod - The converter is freely offered with 
absolutely no warrantee under the GPL public license. <A 
href="http://local-democracy.org/bangla/source.php">Click here</A> to view the 
source code.</I></P>
<P><I>Links: <A href="http://local-democracy.org/bangla/sutonny.php">Sutton 
mapping</A></I></P></BODY></HTML>
